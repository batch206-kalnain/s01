import java.util.Scanner;
public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("First Name:");
        String firstName = scanner.nextLine();

        System.out.println("Last Name:");
        String lastName = scanner.nextLine();

        System.out.println("First Subject Grade:");
        double firstSubject = scanner.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondSubject = scanner.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdSubject = scanner.nextDouble();

        double aveGrade = (firstSubject+secondSubject+thirdSubject)/3;
        System.out.println("Good day,"+firstName+" "+lastName);
        System.out.println("Your average grade is: "+aveGrade);

    }
}