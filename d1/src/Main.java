import java.util.Scanner;
public class Main {
    /*
     *   Main Class is the entry point for our java program.
     *   It is responsible for executing our code.
     *   The main class usually has 1 method inside it, the main() method.
     *   The main method is the method to run our code.
     *
     *   ctrl + / - single comments
     *   ctrl + shift / -multi-line comments
     */
    public static void main(String[] args){
        /*
            Main Method is where most executable code is applied to.

            "public" - is an access modifier which simply tells the application which classes have access to our method/attributes.

            "static" - means that the method/property belongs to the class. This means that it is accessible without having to create an instance of a class.

            "void" - means that this method will not return data. Because in Java we have to declare the data type of the method's return. And since, main does not return data, we add "void".
        */
        System.out.println("Juan Dela Cruz");
        /*
            System.out.println() is a statement which will allow us to print the value of the argument passed into it in our terminal.

            Shortcuts:
                sout+tab = System.out.println()
        */
        System.out.println("I am learning Java");

        /*
            In Java, to be able to declare a variable, we have to identify or declare its data type. Which means, that the variable will expect and only accept with the type declared:
        */
        int myNum;
        myNum = 3;
        System.out.println("The number is: "+myNum);

        myNum = 2500;
        System.out.println("The number is now: "+myNum);

//      Limit of INT data type
        int nationalPopulation = 2147483647;
        System.out.println("National population: "+nationalPopulation);

//        "L" or "l" is added at the end of a long number to recognized as long.
//        Otherwise, it will wrongfully recognize as int,
        long worldPopulation = 7862881145L;
        System.out.println("World Population is: "+worldPopulation);

//        We have to add "f" or "F" at the end of a float to be recognized as float
        float piFloat = 3.141592f;
        System.out.println("PiFloat value is: "+piFloat);

        double piDouble = 3.141592;
        System.out.println("PiDouble value is: "+piDouble);

//      char - can only hold 1 character
        char letter = 'a';
//      Single quote('') -  is char literal
//      Double quote("") - is string literal
        System.out.println(letter);

        boolean isMVP = true;
        boolean isChampion =  false;
        System.out.println("Is MVP? "+isMVP);
        System.out.println("Is Champion? "+isChampion);

//      Constant in Java is declared with final keyword and the data type.
//      final dataType VARIABLENAME
        final int PRINCIPAL = 3000;
        System.out.println("Principal value is: "+PRINCIPAL);

        String username = "yusukeUrameshi91";
        System.out.println(username);

//      String in Java is non-primitive.
//      .isEmpty() us a string method which returns a boolean.
//      It will check the length of the string, return true if the length is 0, return false if otherwise
        System.out.println(username.isEmpty());

        String username2 = "";
        System.out.println(username2.isEmpty());

//      Scanner -  is a class in Java which allows us to create an instance that accepts input from the terminal. It's like prompt() in JS.
//      However, Scanner being a class pre-defined by Java, has to be imported to be used.
        Scanner scannerName = new Scanner(System.in);

//      .nextLine() receives user input and returns a string.
/*
        System.out.println("What is your name?");
        String myName = scannerName.nextLine();
        System.out.println("Your name is: "+myName);
*/

//      .nextInt() receives user input and returns a integer type.
/*
        System.out.println("What is you favorite number:");
        int myFavNum = scannerName.nextInt();
        System.out.println("Your favorite number is: "+myFavNum);
*/

//      .nextDouble() receives user input and returns a double type.
        System.out.println("What is you average grade:");
        double aveGrade = scannerName.nextDouble();
        System.out.println("Your average grade is: "+aveGrade);

//      Much like in JS, Java also has similar mathematical operators.
        System.out.println("Enter the first number:");
        int number1 = scannerName.nextInt();

        System.out.println("Enter the second number:");
        int number2 = scannerName.nextInt();

        int sum = number1 + number2;
        System.out.println("The sum of both number are: "+sum);

        System.out.println("Enter the first number:");
        int number3 = scannerName.nextInt();
        System.out.println("Enter the second number:");
        int number4 = scannerName.nextInt();

        int diff = number3 - number4;
        System.out.println("The difference of number "+number3+" and "+number4+" are: "+diff);
    }

}
